# DryAirControlSystem
A simple system to control a dry air valve, based on a Comet sensor on the network.
The system is based on a raspberry pi0w and a few more components

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.cern.ch/mbaschie/dryaircontrolsystem.git
git branch -M master
git push -uf origin master
```

## List of components
Here and in the docs directory

