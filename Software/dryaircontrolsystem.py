import RPi.GPIO as GPIO
import sys
import time
import requests
import configparser
import os

# Set the configuration file
config_file = ('config.ini')

# check if config file is in the same directory
if os.path.isfile(config_file):
    print(f"'{config_file}' found in the current working directory.")
else:
    print(f"'{config_file}' not found in the current working directory.")
    sys.exit(1)  # Exit the script if the config file is not found

# Create a ConfigParser object
config = configparser.ConfigParser()
# Read the configuration file
config.read(config_file)
# Extract the address from the 'comet' section
comet_address = config['comet']['address']
# Comet sensor address and endpoint
comet_endpoint = f"http://{comet_address}/values.json" #need to implement correct HTTP
# Extract other values from the 'valve' section if needed
valve_threshold = float(config['valve']['threshold'])
valve_hysteresis = float(config['valve']['hysteresis'])

# Debug: Print out the sections to see if they are being read correctly
print(f"Sections in '{config_file}': {config.sections()}")

# Set GPIO mode
GPIO.setmode(GPIO.BOARD)

# Define the GPIO pin numbers
POWER_RELAY_PIN = 15    # GPIO pin connected to the power supply relay
VALVE_RELAY_PIN = 29    # GPIO pin connected to the valve command relay

# Setup the GPIO pins for the relays
GPIO.setup(POWER_RELAY_PIN, GPIO.OUT)
GPIO.setup(VALVE_RELAY_PIN, GPIO.OUT)
GPIO.output(POWER_RELAY_PIN, GPIO.LOW)  # Start with the power relay off
GPIO.output(VALVE_RELAY_PIN, GPIO.LOW)  # Start with the valve relay off

# Desired humidity range with hysteresis
DESIRED_HUMIDITY_MIN = valve_threshold - valve_hysteresis
DESIRED_HUMIDITY_MAX = valve_threshold + valve_hysteresis

print ({DESIRED_HUMIDITY_MIN})

# Boolean flag to track the valve state
bValveOpen = False

def read_humidity():
    #Read humidity from the machine interface over HTTP
    try:
        response = requests.get(comet_endpoint)
        response.raise_for_status()
        data = response.json()
        humidity = float(response.json()['ch2']['aval'])
        return humidity
        #return 44 #DEBUG
    
    except requests.exceptions.RequestException as e:
        print("HTTP request failed:", e)
        return None
    except ValueError as e:
        print("Failed to parse humidity value:", e)
        return None
    except KeyError as e:
        print("Missing expected data in response:", e)
        return None
   
def open_valve():
    #Open the valve using both relays.
    global bValveOpen
    GPIO.output(POWER_RELAY_PIN, GPIO.HIGH)  # Turn on power relay first
    time.sleep(1)                          # Wait 0.5 seconds
    GPIO.output(VALVE_RELAY_PIN, GPIO.HIGH)  # Turn on valve relay
    bValveOpen = True
    print("Opening Valve")


def close_valve():
    #Close the valve using both relays.
    global bValveOpen
    GPIO.output(VALVE_RELAY_PIN, GPIO.LOW)   # Turn off valve relay
    time.sleep(1)                          # Wait 0.5 seconds
    GPIO.output(POWER_RELAY_PIN, GPIO.LOW)
    bValveOpen = False   # Turn off power relay
    print("Closing Valve")


def control_valve(humidity):
    #Control the valve based on the current humidity.
    global bValveOpen

    if humidity < DESIRED_HUMIDITY_MIN:
        if bValveOpen:
            close_valve()
        print(f"Humidity is low ({humidity}%), {bValveOpen}")
    
    elif humidity > DESIRED_HUMIDITY_MAX:
        if not bValveOpen:
            open_valve()
        print(f"Humidity is high ({humidity}%), {bValveOpen}")
    
    else:
        print(f"Humidity is within the desired range ({humidity}%), {bValveOpen}")


try:
    while True:
        humidity = read_humidity()
        if humidity is not None:
            control_valve(humidity)
        time.sleep(5)  # Wait 5 seconds before reading the sensor again

except KeyboardInterrupt:
    print("Program terminated")

finally:
    GPIO.cleanup()
