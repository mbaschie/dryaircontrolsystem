
BCM = 'BCM'
OUT = 'OUT'
HIGH = True
LOW = False

def setmode(mode):
    print(f"setmode({mode})")

def setup(channel, mode):
    print(f"setup(channel={channel}, mode={mode})")

def output(channel, state):
    print(f"output(channel={channel}, state={state})")

def cleanup():
    print("cleanup()")

def setwarnings(flag):
    print(f"setwarnings({flag})")